﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour {

    public Question[] questions;
    private static List<Question> unansweredQuestions;

    private Question currentQuestion;


    [SerializeField]
    private Text QuestionText;
    [SerializeField]
    private float TimeBetweenQuestions = 1f;
    [SerializeField]
    private Text TrueAnswerText;
    [SerializeField]
    private Text FalseAnswerText;
    [SerializeField]
    Animator animator;

    
    
    
    void Start()
    {
        if(unansweredQuestions == null || unansweredQuestions.Count == 0)
        {
            unansweredQuestions = questions.ToList<Question>();
        }

        SetCurrentQuestion();
    }

    void SetCurrentQuestion()
    {
        int randomQuestionIndex = Random.Range(0, unansweredQuestions.Count);
        currentQuestion = unansweredQuestions[randomQuestionIndex];

        QuestionText.text = currentQuestion.fact;

        if(currentQuestion.isTrue)
        {
            TrueAnswerText.text = "CORRECT";
            FalseAnswerText.text = "WRONG";
        }
        else
        {
            TrueAnswerText.text = "WRONG";
            FalseAnswerText.text = "CORRECT";
        }
    }

    IEnumerator TransitionToNextQuestion()
    {
        unansweredQuestions.Remove(currentQuestion);

        yield return new WaitForSeconds(TimeBetweenQuestions);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    public void SelectTrue()
    {
        animator.SetTrigger("True");
        StartCoroutine(TransitionToNextQuestion());
    }

    public void SelectFalse()
    {
        animator.SetTrigger("False");
        StartCoroutine(TransitionToNextQuestion());
    }


}

    


